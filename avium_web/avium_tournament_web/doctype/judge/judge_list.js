frappe.listview_settings['Judge'] = {
    colwidths: {"indicator":1, "subject": 2},
    add_fields: ["status"],
	get_indicator: function(doc) {
	    if(doc.status==="Enabled") {
	        return [__(doc.status), "green", "status,=," + doc.status];
		}
		else if(doc.status==="Disabled"){
        	return [__(doc.status), "red", "status,=," + doc.status];
		}
	}
};