# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, re
from frappe.model.document import Document
from frappe import _

from avium_web import validation


class Judge(Document):

    def before_insert(self):
        self.dni=re.sub('[-]', '', self.dni)
        self.dni=self.dni.upper()


    def validate(self):
        # if not validation.valid_DNI(self.dni):
        #     frappe.throw(frappe._("DNI is not valid"))

        if self.telephone != None and self.telephone != '' and not validation.valid_spanish_phone(self.telephone):
            frappe.msgprint(frappe._("Telephone is not valid"))

        if self.email != None and self.email != '' and not validation.valid_email(self.email):
            frappe.msgprint(frappe._("Email is not valid"))

        if self.mobile_phone != None and self.mobile_phone != '' and not validation.valid_spanish_mobile(
                self.mobile_phone):
            frappe.msgprint(frappe._("Mobile phone is not valid"))

    def before_save(self):
        self.judge_name = self.judge_name.title()