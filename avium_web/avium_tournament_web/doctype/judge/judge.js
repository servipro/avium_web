// Copyright (c) 2017, SERVIPRO SL and contributors
// For license information, please see license.txt

frappe.ui.form.on('Judge', {
	refresh: function(frm) {
	    if(frm.doc.status=="Enabled"){
            frm.add_custom_button(__("Finish Judge"), function() {
                frappe.call({
                    method: "finish_judge",
                    doc:frm.doc,
                    callback: function(r) {
                        show_alert(__('Judge deactivated'))
                        show_alert(__('Groups closed'))
                        frm.reload_doc()
                    }
                })
            })
        }
	    else if(frm.doc.status=="Disabled"){
            frm.add_custom_button(__("Enable Judge"), function() {
                frappe.call({
                    method: "enable_judge",
                    doc:frm.doc,
                    callback: function(r) {
                        show_alert(__('Judge activated'))
                        frm.reload_doc()
                    }
                })
            })
        }
	}
});