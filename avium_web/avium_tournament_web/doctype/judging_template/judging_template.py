# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.website.website_generator import WebsiteGenerator

class JudgingTemplate(WebsiteGenerator):

    def validate(self):
        super(JudgingTemplate, self).validate()

        if self.team_size < 1 or self.team_size > 4:
            frappe.throw(frappe._("Team size of judging template is incorrect. It should be between 1 and 4."))

        for concept in self.concepts:
            # Check the ranges for each concept are correct
            concept.validate()
            # Check the concepts are not repeated
            for comparing_concept in self.concepts:
                if concept.name != comparing_concept.name and concept.concept_name.lower() == comparing_concept.concept_name.lower():
                    frappe.throw((frappe._("Description of concepts {} and {} are the same.")).format(concept.idx, comparing_concept.idx))

        # Check the concepts are not repeated
        for specialisation in self.specialisations:
            for comparing_specialisation in self.specialisations:
                if specialisation.name != comparing_specialisation.name and specialisation.specialisation_name.lower() == comparing_specialisation.specialisation_name.lower():
                    frappe.throw((frappe._("Description of specialisation {} and {} are the same.")).format(specialisation.idx, comparing_specialisation.idx))
            if specialisation.team_size < 1 or specialisation.team_size > 4:
                frappe.throw((frappe._("Team size of the specialization {} is incorrect. It should be between 1 and 4.")).format(specialisation.specialisation_name))

    def make_route(self):
        '''Returns the default route. If `route` is specified in DocType it will be route/title'''
        from_title = self.name
        if self.meta.route:
            return self.meta.route + '/' + from_title
        else:
            return from_title
