// Copyright (c) 2017, SERVIPRO SL and contributors
// For license information, please see license.txt

frappe.ui.form.on('Judging Template', {
	refresh: function(frm) {
                frm.add_fetch('family','cage','cage');
                frm.set_query('family', {'federation': frm.doc.federation});
	}
});

frappe.ui.form.on('Judging Template Specialisation', {
	"specialisations_add": function(frm, cdt, cdn) {
                locals[cdt][cdn]["tufted"] = frm.doc.tufted;
                locals[cdt][cdn]["cage"] = frm.doc.cage;
                frm.refresh_field("specialisations");
	}
});


// TODO update visualization fields when openning document
//frappe.ui.form.on('Judging Template', {
//    onload: function(frm) {
//        // Actualitzem el nom de la família
//        if (frm.doc.family==undefined) {
//            cur_frm.set_value("family_description_visualization", "-");
//        } else {
//            frappe.call({
//                method:"frappe.client.get_value",
//                args: {
//                    doctype:"Family",
//                    filters: {
//                        name:cur_frm.doc.family
//                    },
//                    fieldname:["family_name"]
//                },
//                callback: function(r) {
//                    if (cur_frm.doc.family_description_visualization!=r.message.family_name) {
//                        cur_frm.set_value("family_description_visualization", r.message.family_name);
//                        cur_frm.save();
//                    }
//                }
//            })
//        };
//    }
//});