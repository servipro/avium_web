# -*- coding: utf-8 -*-
# Copyright (c) 2018, SERVIPRO and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class OrderTemplates(Document):
	def import_file(self):
		self.status = "Importing"
		errors = []

		specializations = frappe.db.sql("""
			select specialization.specialisation_name, specialization.name as code
			from `tabJudging Template` as template
			JOIN `tabJudging Template Specialisation` as specialization
			ON specialization.parent = template.name
			where family = '{0}'
		""".format(self.section))

		specializations_cache = {}
		for item in specializations:
			specializations_cache[item[0]]= item[1]

		for line in self.input.split('\n'):
			order, spe =  line.split(",", 1)
			spe = spe.replace("\"","")
			if spe in specializations_cache:
				frappe.db.sql("""
					UPDATE `tabJudging Template Specialisation`
					SET printing_order = {0}
					WHERE name = '{1}'
				""".format(order, specializations_cache[spe]))
			else:
				errors.append(spe + " not found")
		
		self.errors = "\n".join(errors)
		self.status = "Finished"
		self.save()