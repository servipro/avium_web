// Copyright (c) 2018, SERVIPRO and contributors
// For license information, please see license.txt

frappe.ui.form.on('Order Templates', {
	refresh: function(frm) {
		if(frm.doc.status == "Importing"){
            frm.page.set_primary_action(__('Importing'), function () {
				show_alert(__('Please wait'))
			})
		}
		else{
			frm.page.set_primary_action('Import', function () {
				frappe.call({
					method: "import_file",
					doc:frm.doc,
					callback: function(r) {
						show_message(__('Import is in progess'))
						frm.reload_doc()
					}
				})
			})
		}
	}
});