# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and Contributors
# See license.txt
from __future__ import unicode_literals

import frappe
import unittest

from frappe.test_runner import make_test_records

test_records = frappe.get_test_records('Association')

class TestAssociation(unittest.TestCase):
	def setUp(self):
		make_test_records("Association")

	def test_phone(self):
		assoc = frappe.copy_doc(test_records[0])
		assoc.telephone = "666234234"
		assoc.validate()

	def test_phone_not_ok(self):
		assoc = frappe.copy_doc(test_records[0])
		assoc.telephone = "1261234234"
		self.assertRaises(frappe.ValidationError, assoc.validate)

test_dependencies = ["Federation"]