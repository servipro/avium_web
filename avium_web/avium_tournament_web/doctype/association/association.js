// Copyright (c) 2017, SERVIPRO SL and contributors
// For license information, please see license.txt

frappe.ui.form.on('Association', {
	refresh: function(frm) {
        // Filter Territory field
        cur_frm.set_query("territory", function() {
            return {
                "filters": {
                    "has_subterritories": 0
                }
            };
        });
	},
    territory: function(frm) {
        frm.set_value("comunity", "");
        frm.set_value("country", "");

        if (frm.doc.territory) {
            frappe.call({
                method:"avium_web.utils.get_territory_parents",
                args: {
                    'territory': frm.doc.territory
                },
                callback: function(r) {
                    if(r.message[0]!=null){
                        frm.set_value("comunity", r.message[0]);
                    }
                    if(r.message[1]!=null){
                        frm.set_value("country", r.message[1]);
                    }
                }
            });
        }
    },
});
