# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe import _

class JudgingTemplateConcept(Document):
	def validate(self):
		if self.min_value > self.max_value:
			frappe.throw(_("{0} minimum cannot be greater than maximum").format(self.concept_name))

		if self.max_value < self.min_value:
			frappe.throw(_("{0} maximum cannot be lower than maximum").format(self.concept_name))

		if self.default_value!=0 and (self.default_value > self.max_value or self.default_value < self.min_value):
			frappe.throw(_("{0} default value must be between minimum and maximum").format(self.concept_name))