# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from avium_web import validation

class ContactPerson(Document):

    def validate(self):
        if self.contact_telephone!=None and self.contact_telephone!='' and not validation.valid_spanish_phone(self.contact_telephone):
            frappe.msgprint(frappe._("A contact person telephone is not valid"))

        if self.conatct_email!=None and self.conatct_email!='' and not validation.valid_email(self.conatct_email):
            frappe.msgprint(frappe._("A contact person email is not valid"))

        if self.contact_mobile_phone!=None and self.contact_mobile_phone!='' and not validation.valid_spanish_mobile(self.contact_mobile_phone):
            frappe.msgprint(frappe._("A contact person mobile phone is not valid"))