/**
 * Created by pau on 17/10/2016.
 */


    frappe.treeview_settings["Territory"] = {
        breadcrumbs: "Territory",
        title: __("Territory"),
        get_tree_root: true,
        disable_add_node: true,
        get_tree_nodes: "avium_web.avium_tournament_web.doctype.territory.territory.get_children"
    };


