// Copyright (c) 2017, SERVIPRO SL and contributors
// For license information, please see license.txt

frappe.ui.form.on('Territory', {
	refresh: function(frm) {

	},
	setup: function(frm) {
		frm.set_query('parent_territory', {'has_subterritories': 1});
	}
});