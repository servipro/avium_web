# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class Territory(Document):
	pass

@frappe.whitelist()
def get_children():
	doctype = frappe.local.form_dict.get('doctype')
	parent_field = 'parent_' + doctype.lower().replace(' ', '_')
	parent = frappe.form_dict.get("parent")

	if parent==None:
		territories = frappe.db.sql("""select name as value,
			has_subterritories as expandable
			from `tab{ctype}`
			where docstatus < 2
			and {parent_field} IS NULL
			order by name""".format(ctype=frappe.db.escape(doctype), parent_field=frappe.db.escape(parent_field)), as_dict=1)
	else:
		territories = frappe.db.sql("""select name as value,
			has_subterritories as expandable
			from `tab{ctype}`
			where docstatus < 2
			and ifnull(`{parent_field}`,'') = %s
			order by name""".format(ctype=frappe.db.escape(doctype), parent_field=frappe.db.escape(parent_field)),
			parent, as_dict=1)

	return territories