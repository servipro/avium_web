# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

from __future__ import unicode_literals

import frappe
from frappe.model.document import Document

class Family(Document):
	def on_update(self):
		frappe.db.sql("UPDATE `tabJudging Template` SET family_description_visualization = %s where family =%s", [self.family_name, self.name])

	def validate(self):
		# Check printing order
		families = frappe.get_all("Family", fields=["name", "family_name", "printing_order"], filters={"federation": self.federation})

		printing_orders = [family["printing_order"] for family in families
						   if family["name"] != self.name]
		
		families_cache = {family["printing_order"]: (family["name"], family["family_name"])
						  for family in families
						  if family["name"] != self.name}

		if self.printing_order in printing_orders:
			frappe.msgprint(frappe._("Family '{}' has the same printing code").format(families_cache[self.printing_order][1]))

