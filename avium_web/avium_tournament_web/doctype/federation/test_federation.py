# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and Contributors
# See license.txt
from __future__ import unicode_literals

import frappe
import unittest

from frappe.test_runner import make_test_records

test_records = frappe.get_test_records('Federation')

class TestFederation(unittest.TestCase):
	def setUp(self):
		make_test_records("Federation")

	def test_validate(self):
		federation = frappe.copy_doc(test_records[0])
		federation.validate()