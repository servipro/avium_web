# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from avium_web import validation


class JudgeAssociation(Document):

	def validate(self):
		if self.cif != None and self.cif != '':
			self.cif=self.cif.upper()

		if self.telephone!=None and self.telephone!='' and not validation.valid_spanish_phone(self.telephone):
			frappe.msgprint(frappe._("Telephone is not valid"))

		if self.email!=None and self.email!='' and not validation.valid_email(self.email):
			frappe.msgprint(frappe._("Email is not valid"))

		if self.fax!=None and self.fax!='' and not validation.valid_spanish_phone(self.fax):
			frappe.msgprint(frappe._("Fax is not valid"))

		if self.webpage!=None and self.webpage!='' and not validation.valid_website(self.webpage):
			frappe.msgprint(frappe._("Webpage is not valid"))

		if self.mobile_phone!=None and self.mobile_phone!='' and not validation.valid_spanish_mobile(self.mobile_phone):
			frappe.msgprint(frappe._("Mobile phone is not valid"))

		for contact_person in self.contact_person:
			contact_person.validate()
