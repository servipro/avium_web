frappe.form.link_formatters['Judging Template'] = function(value, doc) {
    if(doc.template_name && doc.template_name !== value) {
        return doc.template_name;
    } else {
        return value;
    }
}

frappe.form.link_formatters['Family'] = function(value, doc) {
    if(doc.family_code_visualization && doc.family_code_visualization !== value) {
        return doc.family_code_visualization;
    } else {
        return value;
    }
}