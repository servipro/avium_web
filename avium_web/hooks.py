# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "avium_web"
app_title = "Avium Web"
app_publisher = "SERVIPRO"
app_description = "Avium Main Web"
app_icon = "fa fa-book"
app_color = "red"
app_email = "pau@servipro.eu"
app_license = "MIT"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/avium_web/css/avium_web.css"
app_include_js = "/assets/avium_web/formatters.js"

# include js, css files in header of web template
web_include_css = "/assets/avium_web/css/selectize.css"

web_include_js = [
	"/assets/avium_web/js/selectize.js",
	"/assets/avium_web/js/sentry.js",
	"/assets/avium_web/js/sentry_client.js"
]

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
# doctype_js = {"doctype" : "public/js/doctype.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
home_page = "avium_home"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "avium_web.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "avium_web.install.before_install"
# after_install = "avium_web.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "avium_web.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
#  	"Tournament": "avium_web.utils.has_permission_tournament",
# }

has_website_permission = {
 	"Tournament": "avium_web.utils.has_website_permission",
}

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

scheduler_events = {
	"cron": {
        "0/15 * * * *": [
            "avium_web.utils.collect_statistics"
        ]
	}
}
# 	"all": [
# 		"avium_web.tasks.all"
# 	],
# 	"daily": [
# 		"avium_web.tasks.daily"
# 	],
# 	"hourly": [
# 		"avium_web.tasks.hourly"
# 	],
# 	"weekly": [
# 		"avium_web.tasks.weekly"
# 	]
# 	"monthly": [
# 		"avium_web.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "avium_web.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "avium_web.event.get_events"
# }
after_login = 'avium_web.utils.after_login'

update_website_context = 'avium_web.utils.update_website_context'

#get_website_user_home_page = ['avium_web.utils.get_website_user_home_page']

fixtures = ["Workflow State","Workflow", {"doctype":"Role", "filters":[["name", "like", "AVIUM %"]]}]