# -*- coding: utf-8 -*-
# Copyright (c) 2018, SERVIPRO and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.frappeclient import FrappeClient
from frappe.model.document import Document

class WebsiteCreationSettings(Document):
	def get_client(self, url=None):
		if url is None:
			url=self.url

		return FrappeClient(url, self.username, self.get_password(), verify=False)
