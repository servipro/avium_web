import urllib

import frappe
from frappe.frappeclient import FrappeClient
from frappe.utils import now_datetime
import requests
from requests import ConnectionError
import time
import subprocess

WEBSITE_CREATION_STEPS = ["create_website", "install_apps", "configure_ssl",
                          "check_website_up", "create_migration", "run_migration", "setup_wizard",
                          "setup_tournament", "setup_public_files", "setup_backup", "setup_email"]

WEBSITE_FINISH = "finished"

ASYNC_DEBUG = True


def get_settings():
    settings = frappe.get_doc("Website Creation Settings")
    return settings

def start_website_creation(tournament_name):
    tournament = frappe.get_doc("Tournament", tournament_name)
    if tournament.create_website_status == "" or tournament.create_website_status==None:
        update_tournament_status(tournament_name, WEBSITE_CREATION_STEPS[0])

    execute_current_step(tournament_name, chain_create=True)

def get_next_step(current_step):
    if current_step==WEBSITE_FINISH:
        return WEBSITE_FINISH

    current_step_n = WEBSITE_CREATION_STEPS.index(current_step)

    if current_step_n+1==len(WEBSITE_CREATION_STEPS):
        return WEBSITE_FINISH

    return WEBSITE_CREATION_STEPS[current_step_n+1]

def advance_step(tournament_name):
    tournament = frappe.get_doc("Tournament", tournament_name)
    print("Current step: "+ tournament.create_website_status)
    next_step = get_next_step(tournament.create_website_status)
    update_tournament_status(tournament_name, next_step)
    print("Current step: "+ next_step)

def update_tournament_status(tournament_name, value=""):
    frappe.db.set_value("Tournament", tournament_name, "create_website_status", value, update_modified=False)

def update_tournament_step(tournament_name, step, value=True):
    frappe.db.set_value("Tournament", tournament_name, step, value, update_modified=False)

def execute_current_step(tournament_name, chain_create=False):
    tournament = frappe.get_doc("Tournament", tournament_name)
    current_step = tournament.create_website_status

    if current_step!= WEBSITE_FINISH:
        frappe.enqueue('avium_web.website_creation.utils.'+current_step, tournament_name=tournament_name, chain_create=chain_create)

def create_website(tournament_name, chain_create=False):
    settings = get_settings()
    frappe_client = settings.get_client()

    tournament = frappe.get_doc("Tournament", tournament_name)

    key = now_datetime()
    method = "avium_manager.api.create_site"
    parameters = {
        "site_name": tournament.tournament_url,
        "install_erpnext": False,
        "mysql_password": settings.mysql_password,
        "admin_password": settings.admin_password,
        "key":key
    }

    result = frappe_client.post_api(method, parameters)

    if result["status"] == "ok":
        advance_step(tournament_name)
        update_tournament_step(tournament_name, "site_created", True)
        if chain_create:
            execute_current_step(tournament_name, chain_create)

def install_apps(tournament_name, chain_create=False):
    settings = get_settings()
    frappe_client = settings.get_client()
    tournament = frappe.get_doc("Tournament", tournament_name)

    key = now_datetime()
    method = "avium_manager.api.site_console_command"
    parameters = {
        "site_name": tournament.tournament_url,
        "caller": "install_app",
        "app_name": "avium_tournament",
        "key":key
    }

    result = frappe_client.post_api(method, parameters)

    if result["status"] == "ok":
        advance_step(tournament_name)
        update_tournament_step(tournament_name, "apps_installation", True)
        if chain_create:
            execute_current_step(tournament_name, chain_create)

def configure_ssl(tournament_name, chain_create=False):
    settings = get_settings()
    frappe_client = settings.get_client()

    tournament = frappe.get_doc("Tournament", tournament_name)

    method = "avium_manager.api.set_ssl_site"
    parameters = {
        "site_name": tournament.tournament_url,
    }

    try:
        result = frappe_client.post_api(method, parameters)
    except ConnectionError:
        advance_step(tournament_name)
        if chain_create:
            execute_current_step(tournament_name, chain_create)

def check_website_up(tournament_name, chain_create=False):
    tournament = frappe.get_doc("Tournament", tournament_name)
    response = None

    for i in range(10):
        time.sleep(15)
        try:
            response = requests.get(tournament.get_url()+"/api/method/avium_tournament.api.ping", verify=False)
            if response.status_code == 200:
                break
        except:
            print("Ping retry for {tournament} {i}".format(tournament=tournament_name, i=i))

    if response!=None and response.status_code==200:
        advance_step(tournament_name)
        update_tournament_step(tournament_name, "ssl_certificate", True)
        update_tournament_step(tournament_name, "website_up", True)

        if chain_create:
            execute_current_step(tournament_name, chain_create)

def create_migration(tournament_name, chain_create=False):
    settings = get_settings()
    tournament = frappe.get_doc("Tournament", tournament_name)

    client = settings.get_client(tournament.get_url())


    doc = {
        "doctype" : "Data Migration Connector",
        "connector_name" : "AVIUM Central",
        "connector_type": "Frappe",
        "hostname": settings.dm_url,
        "username": settings.dm_user,
        "password": settings.dm_pass
    }

    client.insert(doc)

    advance_step(tournament_name)
    update_tournament_step(tournament_name, "migration_created", True)

    if chain_create:
        execute_current_step(tournament_name, chain_create)

def run_migration(tournament_name, chain_create=False):
    settings = get_settings()
    tournament = frappe.get_doc("Tournament", tournament_name)
    frappe_client = settings.get_client(tournament.get_url())


    method = "avium_tournament.api.run_migration"
    parameters = {
        "site_name": tournament.tournament_url,
    }

    result = frappe_client.post_api(method, parameters)

    advance_step(tournament_name)
    update_tournament_step(tournament_name, "data_imported", True)


    if chain_create:
        execute_current_step(tournament_name, chain_create)

def setup_wizard(tournament_name, chain_create=False):
    settings = get_settings()
    tournament = frappe.get_doc("Tournament", tournament_name)
    frappe_client = settings.get_client(tournament.get_url())

    method = "avium_tournament.api.setup_full_wizard"
    parameters = {
        "email": settings.wizard_username,
        "password": settings.wizard_password
    }

    result = frappe_client.post_api(method, parameters)

    advance_step(tournament_name)
    update_tournament_step(tournament_name, "setup_wizard", True)


    if chain_create:
        execute_current_step(tournament_name, chain_create)

def setup_oauth(tournament_name, chain_create=False):
    settings = get_settings()
    tournament = frappe.get_doc("Tournament", tournament_name)
    frappe_client = settings.get_client(tournament.get_url())

    client = frappe.new_doc("OAuth Client")
    client.app_name = tournament.tournament_subdomain
    client.redirect_uris = tournament.get_url() + "/api/method/frappe.www.login.login_via_frappe"
    client.default_redirect_uri = client.redirect_uris
    client.skip_authorization = True
    client.save()

    method = "avium_tournament.api.setup_oauth"
    parameters = {
        "client": client.client_id,
        "secret": client.client_secret,
        "url": settings.oauth_server
    }

    result = frappe_client.post_api(method, parameters)

    advance_step(tournament_name)
    update_tournament_step(tournament_name, "setup_oauth", True)

    if chain_create:
        execute_current_step(tournament_name, chain_create)

def setup_tournament(tournament_name, chain_create=False):
    settings = get_settings()
    tournament = frappe.get_doc("Tournament", tournament_name)
    frappe_client = settings.get_client(tournament.get_url())

    method = "avium_tournament.api.setup_tournament"
    parameters = {
        "association": tournament.association
    }

    result = frappe_client.post_api(method, parameters)

    advance_step(tournament_name)
    update_tournament_step(tournament_name, "setup_tournament", True)

    if chain_create:
        execute_current_step(tournament_name, chain_create)

def setup_public_files(tournament_name, chain_create=False):
    settings = get_settings()
    tournament = frappe.get_doc("Tournament", tournament_name)
    frappe_client = settings.get_client(tournament.get_url())

    file = frappe.get_doc("File", settings.public_files_url)

    method = "avium_tournament.api.setup_publicfiles"
    parameters = {
        "zip_file_url": "https://" + settings.avium_domain + file.file_url
    }

    result = frappe_client.post_api(method, parameters)

    advance_step(tournament_name)
    update_tournament_step(tournament_name, "setup_public_files", True)

    if chain_create:
        execute_current_step(tournament_name, chain_create)

def gen_random_letters(n=10):
    import random, string
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(n))

def setup_backup(tournament_name, chain_create=False):
    settings = get_settings()
    tournament = frappe.get_doc("Tournament", tournament_name)
    frappe_client = settings.get_client(tournament.get_url())

    method = "avium_tournament.api.setup_backup"
    parameters = {
        "notify_email": settings.notify_email,
        "frequency": settings.frequency,
        "access_key_id": settings.access_key_id,
        "secret_access_key": settings.secret_access_key,
        "endpoint_url": settings.endpoint_url,
        "bucket": tournament.tournament_subdomain + gen_random_letters().lower(),
        "backup_limit": settings.backup_limit
    }

    result = frappe_client.post_api(method, parameters)

    advance_step(tournament_name)
    update_tournament_step(tournament_name, "setup_backup", True)

    if chain_create:
        execute_current_step(tournament_name, chain_create)

def setup_email(tournament_name, chain_create=False):
    settings = get_settings()
    tournament = frappe.get_doc("Tournament", tournament_name)
    frappe_client = settings.get_client(tournament.get_url())

    method = "avium_tournament.api.setup_email"
    parameters = {
        "domain": settings.email_domain,
        "email_from": settings.email_from,
        "imap_server": settings.imap_server,
        "imap_port": settings.imap_port,
        "smtp_server": settings.smtp_server,
        "smtp_port": settings.smtp_port,
        "user": settings.email_user,
        "password": settings.get_password("email_password")
    }

    result = frappe_client.post_api(method, parameters)

    advance_step(tournament_name)
    update_tournament_step(tournament_name, "setup_email", True)

    if chain_create:
        execute_current_step(tournament_name, chain_create)

def start_website_creation_docker_background(tournament_name):
        tournament = frappe.get_doc("Tournament", tournament_name)

        settings = frappe.get_doc("Website Creation Settings")

        tournament.logs = ""

        frappe.publish_realtime('website_creation_docker', {"progress": [1, 4], "status": "Creando Web"}, doctype="Tournament", docname=tournament_name)

        result = subprocess.run(
                    ["bench", "new-site", tournament.tournament_url, "--no-mariadb-socket", "--db-root-password", settings.docker_mysql_root_password, "--admin-password", settings.docker_admin_password], 
                    cwd="/home/frappe/frappe-bench",
                    stdout = subprocess.PIPE,
                    universal_newlines = True)

        print(result.stdout)
        tournament.logs += result.stdout

        frappe.publish_realtime('website_creation_docker', {"progress": [2, 4], "status": "Instalando Avium"}, doctype="Tournament", docname=tournament_name)

        result = subprocess.run(
                    ["bench", "--site", tournament.tournament_url, "install-app", "avium_tournament"], 
                    cwd="/home/frappe/frappe-bench",
                    stdout = subprocess.PIPE,
                    universal_newlines = True)
                
        print(result.stdout)
        tournament.logs += result.stdout

        frappe.publish_realtime('website_creation_docker', {"progress": [3, 4], "status": "Importando"}, doctype="Tournament", docname=tournament_name)

        subprocess.run(
                ["mkdir", "-p", "/tmp/"+tournament.tournament_url], 
                cwd="/home/frappe/frappe-bench",
                stdout = subprocess.PIPE,
                universal_newlines = True)

        for doc in ["Territory", "Cage", "Family", "Tournament Prizes Description", "Federation", "Association", "Judge Association", "Judging Template", "Translation"]:
            subprocess.run(
                ["bench", "--site", "avium.eu", "export-json", doc,"/tmp/"+tournament.tournament_url+"/"+doc+".json"], 
                cwd="/home/frappe/frappe-bench",
                stdout = subprocess.PIPE,
                universal_newlines = True)
            
        subprocess.run(
            ["bench", "--site", tournament.tournament_url, "import-doc", "/tmp/"+tournament.tournament_url], 
                cwd="/home/frappe/frappe-bench",
                stdout = subprocess.PIPE,
                universal_newlines = True)
        tournament.logs += "Documentos Base Importados"
        print("Documentos Base Importados")


        result = subprocess.run(
                    ["bench", "--site", tournament.tournament_url, "enable-scheduler"], 
                    cwd="/home/frappe/frappe-bench",
                    stdout = subprocess.PIPE,
                    universal_newlines = True)        
        
        tournament.logs += result.stdout
        print(result.stdout)

        frappe.publish_realtime('website_creation_docker', {"progress": [4, 4], "status": "Finalizado"}, doctype="Tournament", docname=tournament_name)

        tournament.db_update()

        return
