from __future__ import unicode_literals
import frappe
import json

from avium_web.utils import get_user_associations

def get_context(context):
    context.no_cache = 1

    associations = get_user_associations(frappe.session.user)

    tournament_fields = ["tournament_name","route", "workflow_state", "name", "tournament_url"]

    tournaments = frappe.get_all("Tournament",
                                 filters=[["association", "in", associations]],
                                 fields=tournament_fields,
                                 order_by="- creation")

    context.show_sidebar = 0

    return {
        "tournaments": tournaments
    }
