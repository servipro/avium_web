from __future__ import unicode_literals
import frappe

no_cache = 1
no_sitemap = 1

def get_context(context):
    tournament_fields = ["tournament_name","route", "association"]
    tournaments = frappe.get_all("Tournament", filters=[{"workflow_state": "Approved"}, {"published": True}], fields=tournament_fields, limit_page_length=100, order_by="start_date")
    finished_tournaments = frappe.get_all("Tournament", filters=[{"workflow_state": "Finished"}, {"published": True}], fields=tournament_fields, limit_page_length=100, order_by="-start_date")

    web_configuration = frappe.get_doc("Web Configuration")

    return {
        "tournaments": tournaments,
        "finished_tournaments": finished_tournaments,
        "homepage_header": web_configuration.homepage_header
    }
