from __future__ import unicode_literals
import frappe
import json


def get_context(context):

    tournament_config = frappe.get_doc("Web Configuration")

    context.show_sidebar = 0

    return {
        "privacy": tournament_config.privacy_policy,
        "cookies": tournament_config.cookies_policy
    }