from __future__ import unicode_literals
import frappe
import json


def get_context(context):
    frappe.local.flags.redirect_location = frappe.form_dict["destintation"]

    if frappe.session.user == "Guest":
        raise frappe.Redirect

    if frappe.form_dict["destintation"] == "register-exhibitor" and len(frappe.get_all("Exhibitor", filters=[["user", "=", frappe.session.user]]))>0:
        frappe.local.flags.redirect_location = "exhibitor_profile"

    if frappe.form_dict["destintation"] == "register-organization" and len(frappe.get_all("Organization", filters=[["user", "=", frappe.session.user]]))>0:
        frappe.local.flags.redirect_location = "my_tournaments"

    raise frappe.Redirect