import frappe
from frappe import _
from datetime import datetime

from avium_web.website_creation.utils import get_settings
from frappe.frappeclient import FrappeClient


def after_login():
    frappe.db.commit()

    roles = frappe.get_roles(frappe.session.user)

    if "System Manager" in roles:
        return

    if "AVIUM Organization" in roles:
        frappe.local.response["home_page"] = "/my_tournaments"
    elif "AVIUM Exhibitor" in roles:
        frappe.local.response["home_page"] = "/"

    return

def get_user_associations(user):
    return list(map(lambda x: x.association, frappe.get_list("Organization", filters=[["user", "=",user]], fields=["association"])))

def has_website_permission(doc, ptype, user, verbose=False):
    associations = get_user_associations(user)

    if "AVIUM Organization" in frappe.get_roles(frappe.session.user):
        if doc.association in associations:
            return True

    return False


def update_website_context(context):
    if "AVIUM Organization" in frappe.get_roles(frappe.session.user):
        context["top_bar_items"].append({
            "right": 0,
            "target": "",
            "url": "/my_tournaments",
            "label": _("My Tournaments")
        })
        context["top_bar_items"].append({
            "right": 0,
            "target": "",
            "url": "/organization_profile",
            "label": _("Organization Profile")
        })
    if "AVIUM Exhibitor" in frappe.get_roles(frappe.session.user):
        context["top_bar_items"].append({
            "right": 0,
            "target": "",
            "url": "/exhibitor_profile",
            "label": _("Exhibitor Profile")
        })
    context["top_bar_items"].append({
        "right": 1,
        "target": "",
        "url": "/contact",
        "label": _("Contact")
    })

def remove_user_if_registered(context):
    if frappe.session.user != "Guest":
        for i, section in enumerate(context.layout[0]["sections"]):
            if "options" in section and "User" == section["options"]:
                context.layout[0]["sections"].pop(i)
    return context


@frappe.whitelist()
def get_territory_parents(territory):

    territory_raw_data = frappe.db.sql(""" 
        SELECT  ter.territory_name as territory,
                parter.territory_name as comunity,
                parparter.territory_name as country
        FROM `tabTerritory` as ter
        LEFT JOIN `tabTerritory` as parter on parter.territory_name = ter.parent_territory
        LEFT JOIN `tabTerritory` as parparter on parparter.territory_name = parter.parent_territory
        """, as_dict=True)

    territory_cache = {
        territory['territory']: (territory['comunity'], territory['country'])
        for territory
        in territory_raw_data
    }

    return territory_cache.get(territory, (None, None))

@frappe.whitelist()
def get_user_details(email, auth_key):
    if auth_key=="4e37eb14-7930-4b97-8190-b6c00564cc67":
        user = frappe.get_doc("User", email)
        
        auth = frappe.db.sql("""select name, `password` from `__Auth`
                where doctype=%(doctype)s and name=%(name)s and fieldname=%(fieldname)s and encrypted=0""",
                { 'doctype': "User", 'name': email, 'fieldname': "password"}, as_dict=True)

       	if not auth:
            frappe.throw("Reinicia tu password en avium.eu")
        else:      
            return {
                "email": user.email,
                "password": auth[0].password,
                "first_name": user.first_name,
                "last_name": user.last_name,
                "username": user.username
            }
    else:
        frappe.throw("Error")

def collect_statistics():
    settings = get_settings()

    for tournament_name in frappe.get_all("Tournament"):
        tournament = frappe.get_doc("Tournament", tournament_name)
        client = FrappeClient("https://"+tournament.tournament_url, settings.username, settings.get_password(), verify=True)

        statistics = client.get_api("avium_tournament.api.get_statistics")

        tournament.individual_groups = int(statistics["individual_groups"])
        tournament.team_groups = int(statistics["team_groups"])
        tournament.registrations = int(statistics["registrations"])
        tournament.individual_specimens = int(statistics["individual_specimens"])
        tournament.team_specimens = int(statistics["team_specimens"])
        tournament.total_templates = int(statistics["total_templates"])
        tournament.pending_templates = int(statistics["pending_templates"])
        tournament.judged_templates = int(statistics["judged_templates"])
        tournament.tied_templates = int(statistics["tied_templates"])
        tournament.error_templates = int(statistics["error_templates"])
        tournament.closed_templates = int(statistics["closed_templates"])
        tournament.last_collected = datetime.now()

        tournament.save()

def fix_users(fix=False):
    reg_requests = frappe.get_all("Registration Request", filters={"type":"Exhibitor"}, fields=["name", "email"])
    exhibitors = [x["user"] for x in frappe.get_all("Exhibitor", fields=["user"])] 

    for item in reg_requests:
        if item["email"] not in exhibitors:
            print(item["email"] + " does not have an exhibitor")
            if fix:
                frappe.db.sql("""delete from `tabOAuth Bearer Token` where user='{user}'""".format(user=item["email"]))
                frappe.delete_doc("User", item["email"])
                rr = frappe.get_doc("Registration Request", item["name"])
                rr.create_user_and_docs()
