# -*- coding: utf-8 -*-
# Copyright (c) 2018, SERVIPRO and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
import re

from frappe.model.document import Document
from frappe import _
from frappe.utils.password import update_password

class RegistrationRequest(Document):

    def before_insert(self):
        self.user = frappe.session.user

        roles = frappe.get_roles()
        if "AVIUM Exhibitor" in roles:
            frappe.throw(_("This account is already registered as exhibitor, please logout to register another account"))

        if frappe.db.exists("User", self.email.strip()) or frappe.db.exists("Registration Request", self.email):
            frappe.throw(_("This email allready exists, please recover your password"))

        if self.user != "Guest":
            self.email = frappe.session.user
            self.email_check = self.email

    def validate(self):
        if self.email.strip() != self.email_check.strip():
            frappe.throw(_("Emails do not match"))

    def after_insert(self):
        web_configuration = frappe.get_doc("Web Configuration")
        if self.type == "Exhibitor" and web_configuration.auto_submit_exhibitors:
            self.create_user_and_docs()

    def on_submit(self):
        self.create_user_and_docs()

    def create_user_and_docs(self):        
        if self.user == "Guest":
            user = frappe.new_doc("User")

            user.email = self.email.strip()
            user.first_name = self.registration_name
            user.last_name = self.surnames
            user.language = self.language

            if self.type == "Exhibitor":
                user.append_roles("AVIUM Exhibitor")
                if not self.breeder_code:
                    frappe.throw(_("Breeder Code is Mandatory"))
                user.username = self.breeder_code
            elif self.type == "Organization":
                user.append_roles("AVIUM Organization")

            user.insert(ignore_permissions=True)
        else:
            user = frappe.get_doc("User", self.user)
            #Use append roles, to be able to save ignoring permissions
            if self.type == "Exhibitor":
                user.append_roles("AVIUM Exhibitor")
            elif self.type == "Organization":
                user.append_roles("AVIUM Organization")
            user.save(ignore_permissions=True)

        if self.type == "Exhibitor":
            
            exhibitor = frappe.new_doc("Exhibitor")
            exhibitor.dni = self.id_card
            exhibitor.exhibitor_name = self.registration_name
            exhibitor.exhibitor_surname = self.surnames

            # Tournament Info
            exhibitor.association = self.association
            self.breeder_code = re.sub('[-\.\ ]', '', self.breeder_code)
            exhibitor.breeder_code = self.breeder_code
            exhibitor.allow = self.allow

            # Address
            exhibitor.address = self.address
            exhibitor.city = self.city
            exhibitor.territory = self.territory
            exhibitor.postal_code = self.postal_code

            # Contact Info
            exhibitor.telephone = self.telephone
            exhibitor.email = self.email.strip()
            exhibitor.mobile_phone = self.mobile_phone

            exhibitor.flags.in_registration = True
            exhibitor.user = user.name
            exhibitor.insert(ignore_permissions=True)
            frappe.db.set_value("Exhibitor", exhibitor.name, "owner", user.name)

        elif self.type == "Organization":
            organization = frappe.new_doc("Organization")
            organization.organization_name = self.registration_name
            organization.association = self.association
            organization.telephone = self.telephone

            organization.flags.in_registration = True
            organization.user = user.name
            organization.insert(ignore_permissions=True)
            frappe.db.set_value("Organization", organization.name, "owner", user.name)

