// Copyright (c) 2017, SERVIPRO SL and contributors
// For license information, please see license.txt

frappe.listview_settings['Exhibitor'] = {
    hide_name_column: true
};