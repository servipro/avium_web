// Copyright (c) 2017, SERVIPRO SL and contributors
// For license information, please see license.txt

frappe.ui.form.on('Exhibitor', {
	refresh: function(frm) {
        frm.add_fetch('association','federation','federation');
	},
	federation: function(frm){
	    var code;
	    if(frm.doc.breeder_code.length>0){
	        code = frm.doc.breeder_code[0]
	    }
	    else{
	        code = frm.add_child('breeder_code');
	    }

	    if(typeof(code.federation_name)==='undefined'){
	        code.federation_name= frm.doc.federation
	    }
	    frm.refresh_field("breeder_code")
	}
});