# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, re
from frappe.model.document import Document
from avium_web import validation


class Exhibitor(Document):

    def on_trash(self):
        reg_reqs = frappe.get_list("Registration Request", filters=[["email", "=", self.owner]])
        for reg in reg_reqs:
            doc = frappe.get_doc("Registration Request", reg["name"])
            doc.delete()


    def after_delete(self):
        user = frappe.get_doc("User", self.owner)
        user.delete()

    def before_insert(self):
        if self.dni:
            self.dni=re.sub('[-\.\ ]', '', self.dni)
            self.dni=self.dni.upper()

    def before_save(self):
        self.exhibitor_name = self.exhibitor_name.title()
        self.exhibitor_surname = self.exhibitor_surname.title()
        self.exhibitor_full_name = self.exhibitor_surname + ", " + self.exhibitor_name

        if self.email:
            self.email = self.email.lower()

        self.breeder_code = re.sub('[-\.\ ]', '', self.breeder_code)
        self.breeder_code = self.breeder_code.upper()

    def validate(self):
        # if not validation.valid_DNI(self.dni):
        #     frappe.throw(frappe._("DNI is not valid"))

        # if self.telephone and not self.telephone.isdigit():
        #     frappe.throw(frappe._("Telephone is not valid"))

        if self.email != None and self.email != '' and not validation.valid_email(self.email):
            frappe.throw(frappe._("Email is not valid"))

        # if self.mobile_phone and not self.mobile_phone.isdigit():
        #     frappe.throw(frappe._("Mobile phone is not valid"))