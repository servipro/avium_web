// Copyright (c) 2018, SERVIPRO and contributors
// For license information, please see license.txt

frappe.listview_settings['Organization'] = {
    hide_name_column: true
};