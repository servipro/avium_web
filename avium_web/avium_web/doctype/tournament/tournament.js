// Copyright (c) 2017, SERVIPRO and contributors
// For license information, please see license.txt

frappe.ui.form.on('Tournament', {
	refresh: function(frm) {
		frappe.realtime.on("website_creation_docker", (data) => {
			console.log('data', data);
			if(data.status) {
				msgprint( data.progress[0] + "/" + data.progress[1] + " - " + data.status)
			}
		})
	},
	view_tournament_website: function(frm){
		var win = window.open("https://"+frm.doc.tournament_url, '_blank');
	}
});