# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.website.website_generator import WebsiteGenerator
import unidecode
import requests
from requests import ConnectionError

from avium_web.website_creation.utils import *
from frappe import _

class Tournament(WebsiteGenerator):

    def before_insert(self):
        if "System Manager" not in frappe.get_roles(frappe.session.user):
            organization = frappe.get_list("Organization", fields=['association'], filters=[["user", "=", frappe.session.user]])

            if len(organization)!=1:
                frappe.throw(_("There are {} organizations configured for {}", [str(len(organization)), frappe.session.user]))

            self.association = organization[0].association

    def autoname(self):
        return self.scrub(unidecode.unidecode(self.tournament_name))

    def make_route(self):
        if not self.route:
            return self.meta.route + '/' + self.scrub(unidecode.unidecode(self.tournament_name))
        else:
            return self.route

    def get_url(self):
        return "https://"+self.tournament_url

    def before_save(self):
        if self.tournament_subdomain and (self.tournament_url=="" or self.tournament_url is None):
            self.tournament_subdomain = self.tournament_subdomain.lower()
            self.set_url()

    def set_url(self):
        settings = frappe.get_doc("Website Creation Settings")
        self.tournament_url = self.tournament_subdomain + "." + settings.avium_domain

    @frappe.whitelist()
    def start_website_creation_docker(self):
        frappe.enqueue('avium_web.website_creation.utils.start_website_creation_docker_background', tournament_name=self.name, 
            job_name=self.tournament_subdomain, 
            queue="short")

    def start_website_creation(self):
        start_website_creation(self.name)

    def create_website(self):
        create_website(self.name)

    def install_apps(self):
        install_apps(self.name)

    def configure_ssl(self):
        configure_ssl(self.name)

    def check_website_up(self):
        check_website_up(self.name)

    def create_migration(self):
        create_migration(self.name)

    def run_migration(self):
        run_migration(self.name)

    def setup_wizard_fn(self):
        setup_wizard(self.name)

    def setup_oauth_fn(self):
        setup_oauth(self.name)

    def setup_tournament_fn(self):
        setup_tournament(self.name)

    def setup_publicfiles_fn(self):
        setup_public_files(self.name)

    def setup_backup_fn(self):
        setup_backup(self.name)
    
    def setup_email_fn(self):
        setup_email(self.name)