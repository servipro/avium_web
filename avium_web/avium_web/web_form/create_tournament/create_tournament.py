from __future__ import unicode_literals

import frappe
from frappe import _

def get_context(context):
	context.parents = [dict(route='/my_tournaments', label=_('My Tournaments'))]
