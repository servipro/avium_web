from __future__ import unicode_literals

from avium_web.utils import remove_user_if_registered

def get_context(context):
    context = remove_user_if_registered(context)
    return context
