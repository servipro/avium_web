# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "Avium Web",
			"color": "red",
			"icon": "fa fa-book",
			"type": "module",
			"label": _("Avium Web")
		},
		{
			"module_name": "Avium Tournament",
			"color": "blue",
			"icon": "fa fa-circle-o-notch",
			"type": "module",
			"label": _("Avium Tournament")
		},
		{
			"module_name": "Website Creation",
			"color": "blue",
			"icon": "fa fa-circle-o-notch",
			"type": "module",
			"label": _("Website Creation")
		}
	]
